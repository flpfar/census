require_relative './census/version'
require_relative './census/utils/api'
require_relative './census/cli'
require_relative './census/controllers/menu_controller'
require_relative './census/controllers/states_controller'
require_relative './census/controllers/cities_controller'
require_relative './census/controllers/names_controller'

require 'byebug'
require 'terminal-table'
