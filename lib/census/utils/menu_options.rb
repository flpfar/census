MENU_OPTIONS = {
  1 => 'Ranking dos nomes mais comuns em um estado',
  2 => 'Ranking dos nomes mais comuns em uma cidade',
  3 => 'Frequência do uso de um nome ao longo dos anos',
  4 => 'Sair'
}.freeze
